#pragma once

// Scalanie
// Na wejscu podajemy tablice oraz poczatek, srodek i koniec tej tablicy
template <typename typ> 
void merge(typ tab[], int poczatek, int srodek, int koniec )
{
	typ *tmp = new typ[((koniec - poczatek) + 1)];   // tablica pomocnicza
	int i = poczatek;
	int j = srodek + 1;
	int k = 0;

	while (i <= srodek && j <= koniec)
	{
		if (tab[j] < tab[i])
		{
			tmp[k] = tab[j];
			j++;
		}
		else
		{
			tmp[k] = tab[i];
			i++;
		}
		k++;
	}

	if (i <= srodek)
	{
		while (i <= srodek)
		{
			tmp[k] = tab[i];
			i++;
			k++;
		}
	}
	else
	{
		while (j <= koniec)
		{
			tmp[k] = tab[j];
			j++;
			k++;
		}
	}
	for (i = 0; i <= koniec - poczatek; i++) {
		tab[poczatek + i] = tmp[i];
	}
	delete[] tmp;
}

/*
	Procedura sortuje podawana jako argument tablice przez scalanie	
	Na wejscu podajemy nieposortowana tablice tab oraz indeks jej poczatku i konca
*/
template <typename typ> 
void MergeSort(typ tab[], int poczatek, int koniec) 
{
	if (poczatek < koniec)
	{
		int srodek = (koniec + poczatek) / 2;
		MergeSort(tab, poczatek, srodek);
		MergeSort(tab, srodek + 1, koniec);
		merge(tab, poczatek, srodek, koniec);
	}
}