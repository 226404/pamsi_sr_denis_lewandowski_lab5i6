#pragma once

// Dzieli tablice na 2 czesci, lewa ma wartosci mniejsze od wybranego piwotu, prawa wieksze
template <typename typ>
int partition(typ tab[], int p, int r)
{
	int piwot = tab[p]; // piwot jest pierwszym elementem tablicy
	int i = p, j = r, tmp; // i, j - indeksy w tabeli
	while (true) // petla nieskonczona - wychodzimy z niej tylko przez return j
	{
		while (tab[j] > piwot) // dopoki elementy sa wieksze od x
			j--;
		while (tab[i] < piwot) // dopoki elementy sa mniejsze od x
			i++;
		if (i < j) // zamieniamy miejscami gdy i < j
		{
			tmp = tab[i];
			tab[i] = tab[j];
			tab[j] = tmp;
			i++;
			j--;
		}
		else // kiedy i >= j zwracane jest j, ktore jest punktem podzialu tablicy
			return j;
	}
}

// Algorytm sortowania szybkiego
// Na wejscie podajemy nieposortowana tablice oraz poczatkowy i koncowy numer jej indeksu
// Skutkiem procedury jest posortowana tablica podana jako argument
template <typename typ>
void quicksort(typ tablica[], int p, int r)
{
	int q;
	if (p < r)
	{
		q = partition(tablica, p, r);	// dzielimy tablice na dwie czesci; q oznacza punkt podzialu
		quicksort(tablica, p, q);		// wywolujemy rekurencyjnie quicksort dla pierwszej czesci tablicy
		quicksort(tablica, q + 1, r);	// wywolujemy rekurencyjnie quicksort dla drugiej czesci tablicy
	}
}
