#pragma once
#include "QuickSort.h"
#include "MergeSort.h"

#include <algorithm>
#include <functional>

#include <iostream>
using namespace std;

// Algorytm sortowania introspektywnego z wykorzystaniem quicksortu oraz
// sortowania o stalej zlozonosci O(n*logn), w tym przypadku mergesort 
template <typename typ>
void IntrospectiveSort(typ *tab, int p, int r, int M)
{
	int q;
	if (M == 0)
		MergeSort(tab, p, r);
	else
	{
		if (p < r)  // quicksort
		{
			q = partition(tab, p, r);			// dzielimy tablice na dwie czesci; q oznacza punkt podzialu
			IntrospectiveSort(tab, p, q, M-1);		// wywolujemy rekurencyjnie introsort dla pierwszej czesci tablicy z pomniejszonym M
			IntrospectiveSort(tab, q + 1, r, M-1);	// wywolujemy rekurencyjnie introsort dla drugiej czesci tablicy z pomniejszonym M
		}
	}

}