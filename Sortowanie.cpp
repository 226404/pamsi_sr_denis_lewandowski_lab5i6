// Sortowanie.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MergeSort.h"
#include "QuickSort.h"
#include "IntrospectiveSort.h"
#include <iostream>
#include <ctime>
#include <fstream>
#include <string>
#include <math.h>

using namespace std;

template <typename typ>
void sortTest(typ *tab, int size)
{
	int j = 0;
	for (int i = 1; i < size; i++)
	{
		if (tab[i - 1] > tab[i])
		{
			cout << "Tablica jest nie posortowana!" << endl;
			return;
		}
	}
	cout << "Tablica jest posortowana prawidlowo!" << endl;
}

// Odwraca tablice
template <typename typ>
void reverseTab(typ *tab, int size)
{	
	typ tmp;
	for (int i = 0; i <= (size / 2); i++)
	{
		tmp = tab[i];
		tab[i] = tab[((size-1) - i)];
		tab[((size - 1) - i)] = tmp;
	}
}

int main()
{
	fstream plik_we, plik_wy;
	string nazwa = "tab1000k_";
	clock_t start, koniec;

	srand(time(NULL));
	int size = 1000000;
	static int tab[1000000];

	plik_wy.open("Badania_1000k_merge 99%", ios::out);
	plik_wy << "1000k merge Sort 99%" << endl << endl;

	int M = (int)floor(2*log2(size));
	for (int i = 1; i <= 100; i++) {
		nazwa = nazwa + to_string(i);

		plik_we.open(nazwa, ios::in);
		for (int j = 0; j < size; j++)
			plik_we >> tab[j];
		
		
		//Sortowanie 99%
		quicksort(tab, 0, (size*99/100));

		start = clock();
//		IntrospectiveSort(tab, 0, size, M);
//		quicksort(tab, 0, size);
		MergeSort(tab, 0, size);
		koniec = clock();
		plik_wy << (koniec - start)/(double)CLOCKS_PER_SEC << endl;

		plik_we.close();
		nazwa.erase(9);
	}
	plik_wy.close();
	cout << "KONIEC 3!" << endl;

	plik_wy.open("Badania_1000k_quick 99%", ios::out);
	plik_wy << "1000k quick Sort 99%" << endl << endl;

	for (int i = 1; i <= 100; i++) {
		nazwa = nazwa + to_string(i);

		plik_we.open(nazwa, ios::in);
		for (int j = 0; j < size; j++)
			plik_we >> tab[j];

		//Sortowanie 99%
		quicksort(tab, 0, (size*99/100));

		start = clock();
//		IntrospectiveSort(tab, 0, size, M);
		quicksort(tab, 0, size);
//		MergeSort(tab, 0, size);
		koniec = clock();
		plik_wy << (koniec - start) / (double)CLOCKS_PER_SEC << endl;

		plik_we.close();
		nazwa.erase(9);
	}
	plik_wy.close();
	cout << "KONIEC 2!" << endl;


	plik_wy.open("Badania_1000k_intro 99%", ios::out);
	plik_wy << "1000k intro Sort 99%" << endl << endl;

	for (int i = 1; i <= 100; i++) {
		nazwa = nazwa + to_string(i);

		plik_we.open(nazwa, ios::in);
		for (int j = 0; j < size; j++)
			plik_we >> tab[j];

		//Sortowanie 99%
		quicksort(tab, 0, (size*99/100));

		start = clock();
		IntrospectiveSort(tab, 0, size, M);
//		quicksort(tab, 0, size);
//		MergeSort(tab, 0, size);
		koniec = clock();
		plik_wy << (koniec - start) / (double)CLOCKS_PER_SEC << endl;

		plik_we.close();
		nazwa.erase(9);
	}

	cout << "KONIEC!" << endl;
	plik_wy.close();
	getchar();
    return 0;
}

